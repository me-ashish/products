import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './main/products/products.component';

const routes: Routes = [
  {path: '', redirectTo: 'products', pathMatch: 'full'},
  // {path: 'products', component: ProductsComponent},
  {path: 'products', loadChildren: () => import('./main/main.module').then(m => m.MainModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
