import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService{
  url: string = 'https://dummyjson.com/products';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.url}`, { observe: 'response' }).pipe(map((response: any) => {
      return response.body.products;
    }), catchError(error => {
      return throwError(() => error);
    }))
  }
}

export interface ProductDto {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  brand: string;
  stock: number;
  rating: number;
  discountPercentage: number;
  thumbnail: string;
  images: string[];
}
