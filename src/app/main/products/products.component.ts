import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ProductDto, ProductsService } from 'src/app/servies/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

  allProducts : ProductDto[] = [];
  allProductsTemp : ProductDto[] = [];
  allCategories: string[] = ['All Products'];
  allSortingOptions: string[] = ['None','Price: Low to High', 'Price: High to Low', 'Name: A-Z', 'Name: Z-A'];
  loading: boolean = true;
  displayDialog: boolean = false;
  form!: FormGroup;

  allProductSubscriptions!: Subscription;
  constructor(
    private _productService: ProductsService,
    private _fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.getAllProducts();
    this.createForm();
  }
  // form for the filter
  createForm(){
    this.form = this._fb.group({
      price: [[0,500]],
      discount: [[0,50]],
      category: ['All Products'],
    })
  }
  // mathod to get All the Products form API
  getAllProducts(){
    this.allProductSubscriptions =this._productService.getProducts().subscribe({
      next: (data) => {
        this.allProducts = data;
        this.allProductsTemp = data;
        this.getAllCategories();
        this.loading = false;
      },
      error: (err) => {
        console.log(err);
      }
    })
  }
  getAllCategories(){
    this.allCategories = ['All Products'];
    this.allProducts.forEach((product) => {
      if (!this.allCategories.includes(product.category)) {
        this.allCategories.push(product.category);
      }
    });
  }

  dialogVisiblity(){
    this.displayDialog = !this.displayDialog;
  }

  getStarArray(value: number){
    let arr: any = [];
    let isodd: boolean = false;
    if((value *2)%2 != 0){
      isodd = true;
    }
    for(let i=1; i<=5; i++){
      if(isodd){
        if(i<Math.ceil(value)){
          arr.push(1);
        } else if(i == Math.ceil(value)){
          arr.push(0.5);
        }else{
          arr.push(0);
        }
      }else{
        if(i<=value){
          arr.push(1);

        }else{
          arr.push(0);
        }
      }
    }

    return arr;
  }

  calculatedValue(value: number, discount: number){
    return value - (value * discount/100);
  }

  searchOnInput(event: any){
    this.displayDialog = false;
    let e = event.target.value;
    if (e) {
          this.allProducts = this.allProductsTemp.filter((type) => {
              return (
                  type.title.toLocaleLowerCase().includes(e.toLocaleLowerCase()) ||
                  type.category.toLocaleLowerCase().includes(e.toLocaleLowerCase())
              );
          });
      } else {
          this.allProducts = this.allProductsTemp;
      }
      this.getAllCategories();
  }

  // filter data after filter button is clicked
  filterData(){
    this.displayDialog = false;
    this.allProducts = this.allProductsTemp.filter((product) => {
      return (
        product.price >= this.form.value.price[0] &&
        product.price <= this.form.value.price[1] &&
        product.discountPercentage >= this.form.value.discount[0] &&
        product.discountPercentage <= this.form.value.discount[1] &&
        (product.category == this.form.value.category || this.form.value.category == 'All Products')
      )
    })
  }
  // method for sorting the products
  sortBy(value: string){
    if(value == 'Price: Low to High'){
      this.allProducts.sort((a,b) => a.price - b.price);
    }else if(value == 'Price: High to Low'){
      this.allProducts.sort((a,b) => b.price - a.price);
    }else if(value == 'Name: A-Z'){
      this.allProducts.sort((a,b) => a.title.localeCompare(b.title));
    }else if(value == 'Name: Z-A'){
      this.allProducts.sort((a,b) => b.title.localeCompare(a.title));
    }else{
      this.allProducts = this.allProducts;
    }
  }

  ngOnDestroy() {
    this.allProductSubscriptions.unsubscribe();
  }

}

