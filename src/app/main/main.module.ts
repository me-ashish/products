import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { MainRoutingModule } from './main.routing.module';
import { SharedModule } from '../shared/shared.module';

import {CardModule} from 'primeng/card';
import { ChipModule } from 'primeng/chip';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {SliderModule} from 'primeng/slider';
import {DropdownModule} from 'primeng/dropdown';

import {DialogModule} from 'primeng/dialog';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,

    CardModule,
    ChipModule,
    ButtonModule,
    InputTextModule,
    SliderModule,
    DropdownModule,


    DialogModule,
    DynamicDialogModule,
  ]
})
export class MainModule { }
